import { defineStore } from "pinia";
import { ref } from "vue";

const usePiniaStore = defineStore("pinia", () => {
  const counter = ref(0);
  const dbcount = ()=>counter.value*2
  const increment = (step=1) => {
    counter.value+=step;
  };
  
  return { counter,dbcount, increment };
});

// const usePiniaStore = defineStore("pinia", {
//   state(){
//     return {
//       counter : 0,
//     }
//   },
//   getters: {
//     dbcount:state => state.counter * 2,
//   },
//   actions: {
//     increment(step = 1) {
//       this.counter +=step
//     }
//   }
  
// });

export {usePiniaStore} ;
