import { defineStore } from "pinia";
import { ref } from "vue";

//类似vuex
// const useStore = defineStore("store", {
//   state: () => {
//     return {
//       tagsList: [],
//       isCollapse: false,
//     };
//   },

//   actions: {
//     setTagsItem(data) {
//       this.tagsList.push(data);
//     },

//     setTagsItem(state,data) {//不用this
//       state.tagsList.push(data);
//     },

//     handleCollapse(data) {
//       this.isCollapse = data;
//     },
//   },
// });

//类似vue3 中的 setup
const useStore = defineStore("store", () => {
  const tagsList = ref([]);
  const isCollapse = ref(false);

  function addTagItem(data) {
    tagsList.value.push(data);
  }

  function delTagItem(index) {
    tagsList.value.splice(index,1)
  }

  function handleCollapse(data) {
    isCollapse.value = data;
  }


  return { tagsList, isCollapse, addTagItem, handleCollapse,delTagItem };
});

export default useStore;
