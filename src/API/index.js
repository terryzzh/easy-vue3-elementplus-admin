import request from "../request";

function getInfo(pagesize,counter) {
	return request.get(`/getInfo/${pagesize}/${counter}`)
}


export { getInfo };