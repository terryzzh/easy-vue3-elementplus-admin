import { createWebHashHistory, createRouter } from "vue-router";

import { start, done } from "nprogress";
import "nprogress/nprogress.css";



const routes = [
  {
    path: "/",
    redirect: "/login",
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/Login.vue"),
    meta: { title: "登录" },
  },
  {
    path: "/home",
    name: "home",
    component: () => import("@/views/Home.vue"),
    redirect: "/dashboard",
    children: [
      {
        path: "/Dashboard",
        name: "dashboard",
        component: () => import("@/views/information/Dashboard.vue"),
        meta: { title: "系统首页" },
      },
      {
        path: "/access",
        name: "access",
        component: () => import("@/views/information/Access.vue"),
        meta: { title: "出入信息" },
      },
      {
        path: "/car",
        name: "car",
        component: () => import("@/views/information/Car.vue"),
        meta: { title: "车辆信息" },
      },
      {
        path: "/pinia",
        name: "pinia",
        component: () => import("@/views/information/Pinia.vue"),
        meta: { title: "pinia" },
      },
      {
        path: "/editor",
        name: "editor",
        component: () => import("@/views/information/Editor.vue"),
        meta: { title: "写点东西" },
      },
      {
        path: "/permission",
        name: "permission",
        component: () => import("@/views/information/Permission.vue"),
        meta: { title: "权限测试",permission:"admin" },
      },
      {
        path: "/403",
        name: "403",
        component: () => import("@/views/403.vue"),
        meta: { title: "403" },
      },
    ],
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  start();
  document.title = `${to.meta.title} | vue3admin`;
  const role = sessionStorage.getItem("username");
  if (!role && to.path !== "/login") {
    next("/login");
  } else if (to.meta.permission) {
    // 简单的模拟管理员权限
    role === "admin" ? next() : next("/403");
  } else {
    next();
  }
});

router.afterEach((to, from) => {
  done();
});

export default router;
