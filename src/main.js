import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";

import router from "./router";


import "./style/index.css";//全局样式


const app = createApp(App);
app.use(router);
app.use(createPinia());//pinia状态管理
app.mount("#app");
