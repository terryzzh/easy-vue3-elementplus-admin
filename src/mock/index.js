export default [
	{
		url: "/getInfo/:pagesize/:page",
		method: "get",
		response: (options) => {
			return {
				code: 200,
				message: "ok",
				count:100,
				data: {
					[`list|${options.query.pagesize}`]: [
						{
							"id|+1": 202005250001,
							name: /豫[A-Z]{3}\d{3}$/,
							entry_time: '@DATETIME("2021-08-dd HH:mm:ss")',
							levea_time: '@NOW("second")',
							"fee|1": [3, 6, 9, 12, 24],
							"grand|1": [0, 1, 2, 3],
						},
					],
				},
				params:options.query
			};
		},
	},
];