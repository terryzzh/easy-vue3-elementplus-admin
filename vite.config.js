import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";



import { resolve } from 'path';

import Components from "unplugin-vue-components/vite";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";
import { viteMockServe } from "vite-plugin-mock";

// https://vitejs.dev/config/
export default defineConfig({
  base: "./",

  // resolve: {
  //   alias: {
  //     "@": resolve("./src"),
  //   },
  // },

  resolve: {
    alias: {
      "@": "/src",
    },
  },

  plugins: [
    vue(),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
    viteMockServe({
      mockPath: "./src/mock",
      logger: true,
    }),
  ],
});
