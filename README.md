# easy-vue3-elementplus-admin

#### 介绍
一个超级简单的vue3+element-plus+vue-router4+pinia 后台管理系统，适合新手学习


#### 安装教程

1. npm install
2. npm run dev


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


